#!/usr/bin/python3

import RPi.GPIO as GPIO
from math import cos,sin
import random
import time

GPIO.setmode(GPIO.BCM)

slp_time = 0.10 ### set sleep time
count = 0 ### Set Count to zero for looping

#------------- Define GPIO settings ------------------
IN=GPIO.IN
OUT=GPIO.OUT

l_ON=GPIO.HIGH
l_OFF=GPIO.LOW

r_ON=GPIO.LOW
r_OFF=GPIO.HIGH

#-------------- Set Layers - Value is GPIO pin ------------------
l1 = 6
l2 = 13
l3 = 19
l4 = 26

#--------------- Set Rows - Value is GPIO pin -------------------
a1 = 17
a2 = 4
a3 = 3
a4 = 2

b1 = 25
b2 = 24
b3 = 23
b4 = 18

c1 = 9
c2 = 10
c3 = 22
c4 = 27

d1 = 16
d2 = 12
d3 = 7
d4 = 8

#---- Define Layers List ----
layers = [l1,l2,l3,l4]
#---- Define Rows List ----
rows = [a1,a2,a3,a4,b1,b2,b3,b4,c1,c2,c3,c4,d1,d2,d3,d4]

#--------------------------- Set GPIO Direction ------------------------------

#---- Set layer pins to output/off ----
for layer in layers:
    GPIO.setup(layer, OUT)
    GPIO.output(layer, l_OFF)

#---- set row pins t output/off ----
for row in rows:
    GPIO.setup(row, OUT)


count = 0 ### Set Count to zero for looping


while count < 100:
    #---- Randomize list for Rows and Layers ----
    rand_row = rows[random.randrange(len(rows))] 
    rand_layer = layers[random.randrange(len(layers))]

    #---- Turn on Random Layer with sleep ----
    GPIO.output(rand_layer, l_ON)
    #---- Turn on Random Row ----
    GPIO.output(rand_row, r_ON)
    #---- Set Sleep Time ----
    time.sleep(slp_time)

    #---- Turn off Random Layer without sleep ----
    GPIO.output(rand_layer, l_OFF)
    #---- Turn off Randowrom ----
    GPIO.output(rand_row, r_OFF)
    
    #---- Increase Count by 1
    count += 1
#---- Reset GPIO pins
GPIO.cleanup()



